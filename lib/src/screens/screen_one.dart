import 'package:flutter/material.dart';

class OneScreen extends StatelessWidget {
  const OneScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {    
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: const Color(0xff22272B),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: const Color(0xff22272B),
        child: Stack(
          children: [
            Column(
              children: [
                const _Logo(),
                const _Image(),
                SizedBox(height: height* 0.05),
                const _Details(),
              ],
            ),
            const _RigthLine(),
            Positioned(
              top: height * 0.686,
              left: width - 70,
              child: const _FloatButtonBack()
            ),
          ],
        ),
      )
    );
  }
}

class _RigthLine extends StatelessWidget {
  const _RigthLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      width: double.infinity,      
      child: CustomPaint(
        painter: _RigthLinePainter(),
      ),   
    );
  }
}

class _FloatButtonBack extends StatelessWidget {
  const _FloatButtonBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 60,      
      decoration: BoxDecoration(        
        boxShadow: <BoxShadow>[
          BoxShadow (color: Colors.black.withOpacity(0.1), offset: const Offset(0, 1), blurRadius: 10)
        ],
      ),
      child: MaterialButton(
        shape: const CircleBorder(),
        color: const Color(0xffFFBE00),
        child: const Padding(
          padding: EdgeInsets.only(left: 5),
          child: Icon(Icons.arrow_back_ios, color: Colors.white, size: 30,),
        ), 
        onPressed: () {  },
      ),
    );
  }
}

class _Logo extends StatelessWidget {
  const _Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;    
    return Container(
      width: double.infinity,
      height: height * 0.15,      
      margin: const EdgeInsets.only(top: 40, left: 150),
      child: RichText(
        text: TextSpan(children: [
          const TextSpan(
              text: 'taxo',
              style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w400, fontFamily: 'Mazzard')),
          WidgetSpan(
            child: Transform.translate(
              offset: const Offset(-2, -5),
              child: const Text(
                '.',                
                textScaleFactor: 0.9,
                style: TextStyle(fontSize: 50, color: Color(0xffFFBE00), fontWeight: FontWeight.w500, fontFamily: 'Mazzard'),          
              ),
            ),
          )
        ]),
      ),
    );
  }
}

class _Image extends StatelessWidget {
  const _Image({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {    
    final double height = MediaQuery.of(context).size.height;
    return Container(        
      width: double.infinity,
      height: height * 0.40,
      padding: const EdgeInsets.only(left: 20, right: 40),
      //child: SvgPicture.asset('assets/images/car.svg'),
      child: const Image(image: AssetImage('assets/images/car.png')),
    );
  }
}

class _Details extends StatelessWidget {
  const _Details({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {      
    return Expanded(
      child: Container(                
        width: double.infinity,
        margin: const EdgeInsets.only(left: 20, right: 40),
        child: Column(          
          crossAxisAlignment: CrossAxisAlignment.start,          
          children: const [
            Text('Multiplied', style: TextStyle(fontSize: 40, color: Color(0xffFFBE00), fontWeight: FontWeight.w400)),
            Text('earnings', style: TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.w400)),
            SizedBox(height: 10,),
            Text('Percive up to 3 times the', style: TextStyle(fontSize: 20, color: Colors.grey, fontWeight: FontWeight.w400)),
            Text('amount of the ride', style: TextStyle(fontSize: 20, color: Colors.grey, fontWeight: FontWeight.w400)),
          ],
        ),
      ),
    );
  }
}

class _RigthLinePainter extends CustomPainter{
  
  @override
  void paint(Canvas canvas, Size size) {
    
    final lapiz = Paint()
        ..color       = const Color(0xffFFBE00)
        ..style       = PaintingStyle.fill
        ..strokeWidth = 3;

    double altura = size.height;
    double ancho = size.width;

     final path = Path();

     path.moveTo(ancho - 25, 0);     
     path.lineTo(ancho - 25, altura * 0.6);
     path.quadraticBezierTo(ancho - 25, altura * 0.6 + 26, ancho - 55, altura * 0.6 + 50);
     path.quadraticBezierTo(ancho - 120, altura * 0.6 + 100, ancho - 55, altura * 0.6 + 150);
     path.quadraticBezierTo(ancho - 25, altura * 0.6 + 170, ancho - 25, altura * 0.6 + 200);
     path.lineTo(ancho - 25, altura);
     path.lineTo(ancho, altura);
     path.lineTo(ancho, 0);

     canvas.drawPath(path, lapiz);    
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
  

}
