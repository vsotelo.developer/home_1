# home_1

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

[Design credits](https://ui8.net/maxim-sirotyuk/products/taxo-ui-kit?rel=pro21)

## Preview
<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/39f9f40a-1713-44ae-b672-32790e2aac92.jpg" alt="Preview" />
